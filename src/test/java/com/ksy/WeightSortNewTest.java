package com.ksy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WeightSortNewTest {

    @Test
    public void BasicTests() {
        System.out.println("****** Basic Tests ******");
        assertEquals("2000 103 123 4444 99", WeightSortNew.orderWeight("103 123 4444 99 2000"));
    }

    @Test
    public void FailTests() {
        assertEquals("11 11 2000 10003 22 123 1234000 44444444 9999", WeightSortNew.orderWeight("2000 10003 1234000 44444444 9999 11 11 22 123"));
    }
}