package com.ksy;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;


public class PickPeaksTest {

    private static String[] msg = {"should support finding peaks",
            "should support finding peaks, but should ignore peaks on the edge of the array",
            "should support finding peaks; if the peak is a plateau, it should only return the position of the first element of the plateau",
            "should support finding peaks; if the peak is a plateau, it should only return the position of the first element of the plateau",
            "should support finding peaks, but should ignore peaks on the edge of the array"};

    private static int[][] array = {{11, 17, 10, 14, 17, -4, -3, 15, -2, 17, 17, 17, 19, -3, 12, 10, 18, 16, 9, 4, -4, 6, -5, 10, 1, 17, 11, 2, 11},
            {14, 14, 3, 4, 2, 18, 14, 17, 2, 17, 17, -2, 10, 2, 15, 12, 3, 11, 19, 16, 19, 7, -5, 19, 14, -3, -1},
            {3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 3},
            {3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 2, 2, 1},
            {2, 1, 3, 1, 2, 2, 2, 2, 1},
            {2, 1, 3, 1, 2, 2, 2, 2},
            {1, 2, 3, 6, 4, 1, 2, 3, 2, 1}};

    private static int[][] posS = {{1, 4, 7, 12, 14, 16, 21, 23, 25},
            {3, 5, 7, 9, 12, 14, 18, 20, 23},
            {3, 7},
            {3, 7, 10},
            {2, 4},
            {2},
            {3, 7}
    };

    private static int[][] peaksS = {{17, 17, 15, 19, 12, 18, 6, 10, 17},
            {4, 18, 17, 17, 10, 15, 19, 19, 19},
            {6, 3},
            {6, 3, 2},
            {3, 2},
            {3},
            {6, 3}};

    @Test
    public void sampleTests() {
        for (int n = 0; n < msg.length; n++) {
            final int[] p1 = posS[n], p2 = peaksS[n];
            Map<String, List<Integer>> expected = new HashMap<String, List<Integer>>() {{
                put("pos", Arrays.stream(p1).boxed().collect(Collectors.toList()));
                put("peaks", Arrays.stream(p2).boxed().collect(Collectors.toList()));
            }},
                    actual = PickPeaks.getPeaks(array[n]);
            assertEquals(msg[n], expected, actual);
        }
    }
}
