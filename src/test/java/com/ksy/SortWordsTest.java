
package com.ksy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SortWordsTest {

    private static final int TIMES = 1000000;
    private static final String TEST_PHRASE = "sort the inner content in descending order";

    @Test
    public void exampleTests() {
        assertEquals("srot the inner ctonnet in dsnnieedcg oredr", SortWords.sortByJoinerAndNewBuilder("sort the inner content in descending order"));
        assertEquals("wiat for me", SortWords.sortByJoinerAndNewBuilder("wait for me"));
        assertEquals("tihs ktaa is esay", SortWords.sortByJoinerAndNewBuilder("this kata is easy"));
        assertEquals("te", SortWords.sortByJoinerAndNewBuilder("te"));
    }

    /*@Test
    public void testSpeed() {
        measurePerformance(() -> SortWords.fastSort(TEST_PHRASE), "fast sort");
        measurePerformance(() -> SortWords.sortByJoinerAndNewBuilder(TEST_PHRASE), "new builder each sort");
        measurePerformance(() -> SortWords.sortByJoinerAndPurgingBuilder(TEST_PHRASE), "one purging builder each sort");
    }

    private void measurePerformance(Runnable runnable, String name) {
        long start = System.nanoTime();
        for (int i = 0; i < TIMES; i++) { runnable.run(); }
        long end = System.nanoTime();
        System.out.println((end - start) + " ns for [" + name + "]");
    }*/
}