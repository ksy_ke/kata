package com.ksy;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class NextBigTest {
    @Test
    public void basicTests() {
        assertEquals(21, NextBig.nextBiggerNumber(12));
        assertEquals(531, NextBig.nextBiggerNumber(513));
        assertEquals(2071, NextBig.nextBiggerNumber(2017));
        assertEquals(441, NextBig.nextBiggerNumber(414));
        assertEquals(414, NextBig.nextBiggerNumber(144));
        assertEquals(534403103, NextBig.nextBiggerNumber(534403031));
        assertEquals(-1, NextBig.nextBiggerNumber(1111));
        //assertEquals(1234567908, NextBig.nextBiggerNumber(144));

    }
}