package com.ksy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;

public class RemovedNumbersTest {
    @Test
    public void test12() {
        List<long[]> res = new ArrayList<>();
        res.add(new long[] {15, 21});
        res.add(new long[] {21, 15});
        List<long[]> a = RemovedNumbers.removNb(26);
        assertArrayEquals(res.get(0), a.get(0));
        assertArrayEquals(res.get(1), a.get(1));
    }

    @Test
    public void test11() {
        List<long[]> res = new ArrayList<>();
        res.add(new long[] {550320, 908566});
        res.add(new long[] {908566, 550320});
        res.add(new long[] {559756, 893250});
        res.add(new long[] {893250, 559756});
        List<long[]> a = RemovedNumbers.removNb(1000003);
        assertArrayEquals(res.get(0), a.get(0));
        assertArrayEquals(res.get(1), a.get(1));
        assertArrayEquals(res.get(2), a.get(2));
        assertArrayEquals(res.get(3), a.get(3));

    }

    @Test
    public void test14() {
        List<long[]> res = new ArrayList<long[]>();
        List<long[]> a = RemovedNumbers.removNb(100);
        assertTrue(res.size() == a.size());
    }
}
