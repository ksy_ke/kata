package com.ksy;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PigLatinTests {
    @Test
    public void testFixedTests() {
        assertEquals("igPay atinlay siay oolcay", PigLatin.pigIt("Pig latin is cool"));
        assertEquals("hisTay siay ymay tringsay", PigLatin.pigIt("This is my string"));
    }

    @Test
    public void oneWord() {
        assertEquals("amnday", PigLatin.pigIt("damn"));
    }

    @Test
    public void oneSymbol() {
        assertEquals("day", PigLatin.pigIt("d"));
    }

 /*   @Test
    public void testPunctuationSymbolsLeftUnmodified() {
        assertEquals("dzAYuvNELPay [ ]QRELGsFplMSyQdRay", PigLatin.pigIt("PdzAYuvNEL [ R]QRELGsFplMSyQd"));
    }

    @Test
    public void testSkippedSpace() {
        assertEquals("fvXuSQyay [ ]vCay", PigLatin.pigIt("yfvXuSQ [ C]v"));
    }

    @Test
    public void testNotPunct() {
        Matcher matcher = Pattern.compile("[^\\p{Punct}]").matcher("[1");
        assertTrue(matcher.find());
        System.out.println(matcher.group());
    }*/

    //    <dzAYuvNELPay [ ]QRELGsFplMSyQdRay>
//    <dzAYuvNELPay []QRELGsFplMSyQdRay>
}