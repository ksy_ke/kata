package com.ksy;

import static java.lang.Math.sqrt;

public class PerfectPower {
    public static int[] isPerfectPower(int n) {
        int maxPower = getMaxPow(n);

        for (int x = 2; x <= sqrt(n); x++) {
            for (int y = 2; y <= maxPower; y++) {
                if (n == Math.pow(x, y)) {
                    return new int[]{x, y};
                }
            }
        }
        return null;
    }

    private static int getMaxPow(int n) {
        int maxPow = 2;
        while (n >= Math.pow(2, maxPow)) {
            maxPow++;
        }
        return maxPow;
    }

    public static void main(String[] args) {
        isPerfectPower(49);
    }
}