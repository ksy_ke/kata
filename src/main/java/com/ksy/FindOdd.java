package com.ksy;

import java.util.HashMap;
import java.util.Map;

public class FindOdd {
    public static int findIt(int[] A) {
        Map<Integer, Integer> valueMap = new HashMap<>();

        for (int current : A) {
            if (valueMap.containsKey(current)) {
                valueMap.put(current, valueMap.get(current) + 1);
            } else {
                valueMap.put(current, 1);
            }
        }

        for (Map.Entry current:valueMap.entrySet()) {
            if ((int)current.getValue() % 2 != 0) return (int)current.getKey();
        }

        return 0;
    }
}