package com.ksy;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.regex.Pattern.matches;

class PigLatin {
    static String pigIt(String str) {

        if (str == null || str.equals("")) {
            return "";
        }

        String[] words = str.split("\\s");

        StringJoiner joiner = new StringJoiner(" ");
        for (String word : words) {
            char firstLiteral = word.charAt(0);
            joiner.add(word.substring(1) + firstLiteral + "ay");
        }

        return joiner.toString();
    }

    static String pigItK(String str) {
        return Optional.ofNullable(str)
                .filter(w -> !w.isEmpty())
                .map(w -> w.split(" "))
                .map(Arrays::asList)
                .orElse(emptyList())
                .stream()
                .map(w -> {
                    Pattern.compile("^\\p{Punct}");
                    char firstChar = w.charAt(0);
                    return w.substring(1) + firstChar;
                })
                .map(word -> Optional.of(word)
                        .filter(w -> !(w.length() == 1 && matches("\\p{Punct}", w)))
                        .map(s -> s + "ay")
                        .orElse(word)
                )
                .collect(Collectors.joining(" "));
    }
}
