package com.ksy;

import static java.lang.Character.getNumericValue;
import static java.lang.Math.pow;


class DigPow {
    static long digPow(int passedNumber, int power) {
        int result = 0;
        for (char charOfNumeral : Integer.toString(passedNumber).toCharArray()) {
            result += (int) pow(getNumericValue(charOfNumeral), power++);
        }
        return (result % passedNumber == 0) ?
                result / passedNumber :
                -1;
    }
}