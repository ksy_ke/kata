package com.ksy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

public class ExpandedForm {
    public static String expandedForm(int num)
    {
        int length = String.valueOf(num).length();
        List<Integer> list = new ArrayList<>();

        for (int i = 1; i < length; i++) {
            int remainder = num % (int)Math.pow(10, i);
            if (remainder != 0) {
                list.add(remainder);
                num -= remainder;
            }
        }

        Collections.reverse(list);

        StringJoiner result = new StringJoiner(" + ");
        result.add(String.valueOf(num));

        for (int current : list) {
            result.add(String.valueOf(current));
        }

        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(expandedForm(70304));
    }
}
