package com.ksy;

/* http://www.codewars.com/kata/55c6126177c9441a570000cc/train/java */

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WeightSort {

    public static String orderWeight(String strng) {
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(strng);

        Map<String, Integer> map = new HashMap<>();
        Set<String> whatIsAvailable = new HashSet<>();
        List<String> duplicates = new ArrayList<>();


        while (m.find()) {
            String current = strng.substring(m.start(), m.end());

            if (!whatIsAvailable.add(current)) {
                duplicates.add(current);
            }

            char[] num = current.toCharArray();
            int value = 0;

            for (char aNum : num) {
                value += Integer.parseInt(String.valueOf(aNum));
            }

            map.put(current, value);
        }

        List<Map.Entry<String, Integer>> sorted = new ArrayList<>(map.entrySet());
        sorted.sort(Map.Entry.<String, Integer>comparingByValue().thenComparing(Map.Entry.comparingByKey()));

        StringJoiner result = new StringJoiner(" ");
        for (Map.Entry<String, Integer> entry : sorted) {
            result.add(entry.getKey());
            for (String dub : duplicates) {
                if (dub.equals(entry.getKey())) result.add(entry.getKey());
            }
        }

        return result.toString();
    }
}