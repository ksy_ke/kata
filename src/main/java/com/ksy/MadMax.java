package com.ksy;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

class MadMax {
    void createRandomArray() {
        ThreadLocalRandom random = ThreadLocalRandom.current();

        int arrayLength = random.nextInt(1, 128);
        if (arrayLength % 2 == 0) arrayLength += 1;

        Set<Integer> numberSet = new TreeSet<>();

        while (numberSet.size() < arrayLength) {
            numberSet.add(random.nextInt(0, 256));
        }

        Integer[] randomNumberArray = numberSet.toArray(new Integer[arrayLength]);

        sortArray(randomNumberArray);
    }

    void sortArray(Integer[] randomNumberArray) {
        int arrayLength = randomNumberArray.length;
        for (int left = arrayLength / 2 + 1, right = arrayLength - 1; right > left; left++, right--) {
            int temp = randomNumberArray[left];
            randomNumberArray[left] = randomNumberArray[right];
            randomNumberArray[right] = temp;
        }

       /* do otherwise:
       Arrays.sort(randomNumberArray, arrayLength / 2, arrayLength, Comparator.reverseOrder());
       */
    }
}